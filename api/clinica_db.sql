
-- Matando todas as conexão para esse banco, problema do carai
REVOKE CONNECT ON DATABASE clinicadb FROM PUBLIC, username;

SELECT 
    pg_terminate_backend(pid) 
FROM 
    pg_stat_activity 
WHERE 
    pid <> pg_backend_pid()
    AND datname = 'clinicadb'
    ;

-- Criação do banco de dados
drop database clinicadb;
drop user clinica;
create user clinica with password 'clinica2021';
create database clinicadb with template=template0 owner=clinica;

\connect clinicadb; 

alter default privileges grant all on tables to clinica;
alter default privileges grant all on sequences to clinica;


create table usuarios(
    usuario_id integer primary key not null,
    senha text not null
); 

create table pacientes(
    paciente_id integer primary key not null,
    nome text not null,
    cpf varchar(14) not null,
    uf varchar(2) not null, 
    data_nascimento TIMESTAMP null,
    peso decimal(10,2) null,
    altura decimal(10,2) null
);

create table colaboradores(
    colaborador_id integer primary key not null,
    nome text not null,
    cpf varchar(14) not null,
    usuario_id integer not null
);

create table medicos(
    medico_id integer primary key not null,
    colaborador_id integer not null
); 

create table enfermeiros(
    enfermeiro_id integer primary key not null,
    colaborador_id integer not null
); 

create table roles(
    role_id integer primary key not null,
    nome text not null,
    usuario_id integer not null
); 


alter table colaboradores add constraint col_use_fk
foreign key (usuario_id) references usuarios(usuario_id);

alter table medicos add constraint med_col_fk
foreign key (colaborador_id) references colaboradores(colaborador_id); 

alter table enfermeiros add constraint enf_col_fk
foreign key (colaborador_id) references colaboradores(colaborador_id);  
 
alter table roles add constraint rol_user_fk
foreign key (usuario_id) references usuarios(usuario_id);  

create sequence usuarios_seq increment 1 start 1;
create sequence pacientes_seq increment 1 start 1;
create sequence colaboradores_seq increment 1 start 1;
create sequence medicos_seq increment 1 start 1; 
create sequence enfermeiros_seq increment 1 start 1;
create sequence roles_seq increment 1 start 1;