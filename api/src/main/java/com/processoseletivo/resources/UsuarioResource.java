package com.processoseletivo.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.processoseletivo.domain.Usuario;
import com.processoseletivo.services.ColaboradorService;
import com.processoseletivo.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController 
@RequestMapping("/api/usuarios")
public class UsuarioResource {

    @Autowired 
    UsuarioService usuarioService;
    @Autowired 
    ColaboradorService colaboradorService;

    private final PasswordEncoder encoder;


    public UsuarioResource(PasswordEncoder encoder) {
        this.encoder = encoder;
    }
    
    @PostMapping("/adicionar")
    public ResponseEntity<Map<String, Object>> adicionarUsuario(@RequestBody Map<String, Object> usuarioPost) {
        Map<String, Object> map = new HashMap<>(); 
        try {
            Usuario u = new Usuario(
                null, 
                encoder.encode((String) usuarioPost.get("password")),
                (String) usuarioPost.get("login"),
                (String) usuarioPost.get("cpf")
            );  

            Integer setor = (Integer) usuarioPost.get("setor");
    
            Usuario usuario = usuarioService.adicionarUsuario(u, setor);            

            if ( usuario != null) {    
                map.put("success", true); 
                map.put("content", "Adicionado com sucesso"); 
                return new ResponseEntity<>(map, HttpStatus.OK);  
            } else {
                map.put("success", false); 
                map.put("content", "Não foi possível adicionar, o usuário não válido");  
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST); 
            }
    
        } catch (Exception e) {
            map.put("success", false);  
            map.put("content", e.toString()); 
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
    } 

    @GetMapping("/listar") 
    public ResponseEntity<Map<String, Object>> listarUsuarios() {

        List<Usuario> usuarios = usuarioService.get_all();            


        Map<String, Object> map = new HashMap<>(); 
        map.put("success", true); 
        map.put("content", usuarios);  
        return new ResponseEntity<>(map, HttpStatus.OK);  
    }
}
