package com.processoseletivo.services;

import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;

public interface ColaboradorService { 
    Integer adicionarColaborador(Usuario u) throws CliAuthException; 
    Integer setMedico(Integer colaborador_id) throws CliAuthException;
    Integer setEnfermeiro(Integer colaborador_id) throws CliAuthException; 
}
