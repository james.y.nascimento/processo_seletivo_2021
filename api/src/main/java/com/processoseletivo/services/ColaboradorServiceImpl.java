package com.processoseletivo.services;

import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;
import com.processoseletivo.repositories.ColaboradorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ColaboradorServiceImpl implements ColaboradorService{

    @Autowired
    ColaboradorRepository colaboradorRepository;
    
    @Override
    public Integer adicionarColaborador(Usuario c) throws CliAuthException {
        return colaboradorRepository.create(c);   
    }

    @Override
    public Integer setMedico(Integer colaborador_id) throws CliAuthException {
        return colaboradorRepository.setMedico(colaborador_id); 
    }

    @Override
    public Integer setEnfermeiro(Integer colaborador_id) throws CliAuthException {
        return colaboradorRepository.setEnfermeiro(colaborador_id); 
    } 
}
