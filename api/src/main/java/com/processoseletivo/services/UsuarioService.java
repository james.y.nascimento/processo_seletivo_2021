package com.processoseletivo.services;
import java.util.ArrayList;
import java.util.List;

import com.processoseletivo.domain.Role;
import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;

public interface UsuarioService {

    Usuario adicionarUsuario(Usuario u, Integer setor) throws CliAuthException;
    
    Usuario findById(Integer id) throws CliAuthException; 

    List<Usuario> get_all() throws CliAuthException; 

    Role adicionarRole(Role role) throws CliAuthException; 

    void adicionaRoleUsuario(String login, String roleNome) throws CliAuthException; 

    ArrayList<Role> get_all_roles_by_usuario_id(Integer usuario_id) throws CliAuthException; 


}
