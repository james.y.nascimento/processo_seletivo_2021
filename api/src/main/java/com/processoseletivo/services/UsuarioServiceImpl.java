package com.processoseletivo.services;

import java.util.ArrayList;
import java.util.List;

import com.processoseletivo.domain.Role;
import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;
import com.processoseletivo.repositories.RoleRepository;
import com.processoseletivo.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UsuarioServiceImpl implements UsuarioService{

    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired 
    ColaboradorService colaboradorService;
    @Autowired
    RoleRepository roleRepository;

    // todo, criar arquivo config para essas constantes
    public static final int MEDICO = 1;
    public static final int ENFERMEIRO = 2;

    @Override
    public Usuario adicionarUsuario(Usuario u, Integer setor) throws CliAuthException {
        if ( ehValidoUsuario(u, setor) ) {
            Integer usuario_id = usuarioRepository.create(u); 
            u.setUsuario_id(usuario_id);
            Integer colaborador_id = colaboradorService.adicionarColaborador(u);

            if ( setor == MEDICO ) {
                colaboradorService.setMedico(colaborador_id);
                Role role = new Role(-1, "ROLE_MEDICO", usuario_id);
                roleRepository.create(role);
            }

            if ( setor == ENFERMEIRO ) {
                colaboradorService.setEnfermeiro(colaborador_id);
                Role role = new Role(-1, "ROLE_ENFERMEIRO", usuario_id);
                roleRepository.create(role);
            }


            return usuarioRepository.findById(usuario_id);
        } else {
            return null; 
        }        
    }

    private boolean ehValidoUsuario(Usuario u, Integer setor) {
        boolean valido = true;
        if (u.getPassword().trim().length() == 0) {
            valido = false;
            throw new CliAuthException("Senha não pode ser vázia");
        }

        Integer existeCpf = usuarioRepository.getCountByCpf(u.getCpf());

        if ( existeCpf  > 0 ) { 
            valido = false;
            throw new CliAuthException("Já exite um usuário com esse cpf");
        } 

        Integer existeNome = usuarioRepository.getCountByNome(u.getLogin());

        if ( existeNome  > 0 ) { 
            valido = false;
            throw new CliAuthException("Já exite um usuário com esse nome");
        } 

        if ( setor != MEDICO && setor != ENFERMEIRO ) {
            valido = false;
            throw new CliAuthException("Setor Inválido");
        }

        return valido; 

    }

    @Override
    public Usuario findById(Integer id) throws CliAuthException {
        Usuario usuario = usuarioRepository.findById(id);
        ArrayList<Role> roles = roleRepository.get_all_roles_by_usuario_id(id);
        usuario.setRoles(roles);
        return usuario;
    }

    @Override
    public List<Usuario> get_all() throws CliAuthException {
        List<Usuario> usuarios = usuarioRepository.get_all();
        for (Usuario usuario : usuarios) {
            ArrayList<Role> roles = roleRepository.get_all_roles_by_usuario_id(usuario.getUsuario_id());
            usuario.setRoles(roles);
        }
        return usuarios;
    }

    @Override
    public Role adicionarRole(Role role) throws CliAuthException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void adicionaRoleUsuario(String login, String roleNome) throws CliAuthException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public ArrayList<Role> get_all_roles_by_usuario_id(Integer usuario_id) throws CliAuthException {
        // TODO Auto-generated method stub
        return null;
    }
    
}
