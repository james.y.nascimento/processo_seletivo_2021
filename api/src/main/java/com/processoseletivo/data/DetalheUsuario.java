package com.processoseletivo.data;

import java.util.ArrayList;
import java.util.Collection;

import com.processoseletivo.domain.Usuario;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class DetalheUsuario implements UserDetails{

    private final Usuario usuario;

    public DetalheUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public String getPassword() {
        if ( usuario == null ) {
            return "";
        }
        return usuario.getPassword(); 
    }

    @Override
    public String getUsername() {
        if ( usuario == null ) {
            return "";
        } 
        return usuario.getLogin(); 
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true; 
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }
    
}
