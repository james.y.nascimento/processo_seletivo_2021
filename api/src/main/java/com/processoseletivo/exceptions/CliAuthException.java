package com.processoseletivo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class CliAuthException extends RuntimeException {
    
    public CliAuthException(String mensagem) {
        super(mensagem);
    }

    @Override
    public String toString() {
        return this.getMessage(); 
    }
}
 