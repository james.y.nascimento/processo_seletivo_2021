package com.processoseletivo.repositories;

import java.sql.PreparedStatement;
import java.sql.Statement;
import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class ColaboradorRepositoryImpl implements ColaboradorRepository{

    private static final String SQL_CREATE = "INSERT INTO colaboradores(colaborador_id, nome, cpf, usuario_id) VALUES(NEXTVAL('colaboradores_seq'), ?, ?, ?)";
    private static final String SQL_CREATE_MEDICO = "INSERT INTO medicos(medico_id, colaborador_id) VALUES(NEXTVAL('medicos_seq'), ?)";
    private static final String SQL_CREATE_ENFERMEIRO = "INSERT INTO enfermeiros(enfermeiro_id, colaborador_id) VALUES(NEXTVAL('enfermeiros_seq'), ?)";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Integer create(Usuario c) throws CliAuthException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, c.getLogin().toLowerCase()); 
                ps.setString(2, c.getCpf()); 
                ps.setInt(3, c.getUsuario_id());
                return ps;
            }, keyHolder);
            Integer colaborador_id = (Integer) keyHolder.getKeys().get("colaborador_id");
            return colaborador_id;
        } catch (Exception e) {
            throw new CliAuthException("Falha em criar o colaborador. ".concat(e.getMessage()));
        }
    }

    @Override
    public Integer setMedico(Integer colaborador_id) throws CliAuthException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE_MEDICO, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, colaborador_id);
                return ps;
            }, keyHolder);
            Integer medico_id = (Integer) keyHolder.getKeys().get("medico_id");
            return medico_id; 
        } catch (Exception e) {
            throw new CliAuthException("Falha em criar o medico. ".concat(e.getMessage()));
        }
        
    }

    @Override
    public Integer setEnfermeiro(Integer colaborador_id) throws CliAuthException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE_ENFERMEIRO, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, colaborador_id);
                return ps;
            }, keyHolder);
            Integer enfermeiro_id = (Integer) keyHolder.getKeys().get("enfermeiro_id");
            return enfermeiro_id;
        } catch (Exception e) {
            throw new CliAuthException("Falha em criar o enfermeiro. ".concat(e.getMessage()));
        }
        
    }
    
}
