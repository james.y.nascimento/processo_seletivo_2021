package com.processoseletivo.repositories;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;

import com.processoseletivo.domain.Role;
import com.processoseletivo.exceptions.CliAuthException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl implements RoleRepository{

    private static final String SQL_CREATE = "INSERT INTO roles(role_id, nome, usuario_id) VALUES(NEXTVAL('roles_seq'), ?, ?)";
    private static final String SQL_GET_ALL_ROLES_USUARIO = "SELECT * FROM roles where usuario_id = ?";

    @Autowired 
    JdbcTemplate jdbcTemplate;

    @Override
    public Role findByNome(String nome) throws CliAuthException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ArrayList<Role> get_all_roles_by_usuario_id(Integer usuario_id) throws CliAuthException {
        try { 
            ArrayList<Role> roles = (ArrayList<Role>) jdbcTemplate.query(SQL_GET_ALL_ROLES_USUARIO, roleRowMapper, new Object[] { usuario_id });
            return roles; 

        } catch (Exception e) {
            throw new CliAuthException(e.toString());
        }
    }

    private RowMapper<Role> roleRowMapper = ((rs, rowNum) -> {
        
        try {
            return new Role(
                rs.getInt("role_id"), 
                rs.getString("nome"), 
                rs.getInt("usuario_id")
            ); 
        } 
        catch (Exception e) { 
            throw new CliAuthException(e.toString());
        } 
    });

    @Override
    public Integer create(Role u) throws CliAuthException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder(); 
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, u.getNome()); 
                ps.setInt(2, u.getUsuario_id()); 
                return ps;
            }, keyHolder);
            return (Integer) keyHolder.getKeys().get("usuario_id");
        } catch (Exception e) {
            throw new CliAuthException("Falha em criar a role. ".concat(e.toString()));
        }
    }
    
}
