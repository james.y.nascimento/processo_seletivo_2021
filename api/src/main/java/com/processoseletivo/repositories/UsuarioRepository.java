package com.processoseletivo.repositories;

import java.util.List;

import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;

public interface UsuarioRepository {
    
    Integer create(Usuario u) throws CliAuthException;

    Usuario findByNome(String nome) throws CliAuthException;

    Integer getCountByNome( String nome) throws CliAuthException;
 
    Usuario findById(Integer usuario_id) throws CliAuthException;

    Integer getCountByCpf(String cpf) throws CliAuthException;

    List<Usuario> get_all() throws CliAuthException; 
    
} 
