package com.processoseletivo.repositories;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepository{

    private static final String SQL_CREATE = "INSERT INTO usuarios(usuario_id, senha) VALUES(NEXTVAL('usuarios_seq'), ?)";
    private static final String SQL_COUNT_BY_NOME = "SELECT COUNT(*) FROM usuarios AS U JOIN colaboradores AS C ON U.usuario_id = C.usuario_id WHERE C.nome = ?";
    private static final String SQL_COUNT_BY_CPF = "SELECT COUNT(*) FROM usuarios AS U JOIN colaboradores AS C ON U.usuario_id = C.usuario_id WHERE C.cpf = ?";
    private static final String SQL_BY_ID = "SELECT U.usuario_id, U.senha, C.colaborador_id, C.nome, C.cpf FROM usuarios AS U JOIN colaboradores AS C ON U.usuario_id = C.usuario_id WHERE U.usuario_id = ?";
    private static final String SQL_BY_NOME = "SELECT U.usuario_id, U.senha, C.colaborador_id, C.nome, C.cpf FROM usuarios AS U JOIN colaboradores AS C ON U.usuario_id = C.usuario_id WHERE C.nome = ? limit 1";
    private static final String SQL_GET_ALL = "SELECT U.usuario_id, U.senha, C.colaborador_id, C.nome, C.cpf FROM usuarios AS U JOIN colaboradores AS C ON U.usuario_id = C.usuario_id";


    @Autowired 
    JdbcTemplate jdbcTemplate;

    @Override
    public Integer create(Usuario usuario) throws CliAuthException {
        try {
            String senha = usuario.getPassword();
            KeyHolder keyHolder = new GeneratedKeyHolder(); 
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, senha); 
                return ps;
            }, keyHolder);
            return (Integer) keyHolder.getKeys().get("usuario_id");
        } catch (Exception e) {
            throw new CliAuthException("Falha em criar o usuário. ".concat(e.toString()));
        }
    }

    @Override
    public Usuario findByNome(String nome) throws CliAuthException {
        try { 
            List<Usuario> usuarios = jdbcTemplate.query(SQL_BY_NOME, usuarioRowMapper, new Object[] { nome });
            return usuarios.isEmpty() ? null : usuarios.get(0); 

        } catch (Exception e) {
            throw new CliAuthException(e.toString());
        }
    } 

    @Override
    public Integer getCountByNome(String nome) throws CliAuthException {
        try {
            return jdbcTemplate.queryForObject(SQL_COUNT_BY_NOME, Integer.class, nome);
        } catch (Exception e) {
            throw new CliAuthException(e.toString());
        }
    }

    @Override
    public Usuario findById(Integer usuario_id) throws CliAuthException {
        try {
            return jdbcTemplate.queryForObject(SQL_BY_ID, usuarioRowMapper, new Object[] { usuario_id });
        } catch (Exception e) {
            throw new CliAuthException(e.toString());
        }
    }

    private RowMapper<Usuario> usuarioRowMapper = ((rs, rowNum) -> {
        
        try {
            return new Usuario(
                rs.getInt("usuario_id"), 
                rs.getString("senha"), 
                rs.getString("nome"),
                rs.getString("cpf")
            ); 
        } 
        catch (Exception e) { 
            throw new CliAuthException(e.toString());
        } 
    });

    @Override
    public Integer getCountByCpf(String cpf) {
        try {
            return jdbcTemplate.queryForObject(SQL_COUNT_BY_CPF, Integer.class, cpf);
        } catch (Exception e) { 
            throw new CliAuthException(e.toString()); 
        }
    }

    @Override
    public List<Usuario> get_all() throws CliAuthException {
        try { 
            List<Usuario> usuarios = jdbcTemplate.query(SQL_GET_ALL, usuarioRowMapper);
            return usuarios; 

        } catch (Exception e) {
            throw new CliAuthException(e.toString());
        }
    }

    
    
}
