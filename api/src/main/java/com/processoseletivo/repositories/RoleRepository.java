package com.processoseletivo.repositories;

import java.util.ArrayList;

import com.processoseletivo.domain.Role;
import com.processoseletivo.exceptions.CliAuthException;

public interface RoleRepository {
    Integer create(Role u) throws CliAuthException; 
    Role findByNome(String nome) throws CliAuthException;
    ArrayList<Role> get_all_roles_by_usuario_id(Integer usuario_id) throws CliAuthException; 
}
