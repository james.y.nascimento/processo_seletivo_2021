package com.processoseletivo.repositories;

import com.processoseletivo.domain.Usuario;
import com.processoseletivo.exceptions.CliAuthException;

public interface ColaboradorRepository {
    Integer create(Usuario c) throws CliAuthException;
    Integer setMedico(Integer colaborador_id) throws CliAuthException;
    Integer setEnfermeiro(Integer colaborador_id) throws CliAuthException;
}
