package com.processoseletivo.domain;

public class Role {
    private Integer role_id;
    private String nome;
    private Integer usuario_id;

    public Role(Integer role_id, String nome, Integer usuario_id) {
        this.setRole_id(role_id);
        this.setNome(nome);
        this.setUsuario_id(usuario_id); 
    }

    
    public Integer getUsuario_id() {
        return usuario_id;
    }


    public void setUsuario_id(Integer usuario_id) {
        this.usuario_id = usuario_id;
    }


    public Integer getRole_id() {
        return role_id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }
}
