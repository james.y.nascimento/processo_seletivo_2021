package com.processoseletivo.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Usuario {
    
    private Integer usuario_id;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String login;
    private String cpf;
    private ArrayList<Role> roles;

    public Usuario(Integer usuario_id, String password, String login, String cpf) {
        this.setUsuario_id(usuario_id);
        this.setPassword(password);
        this.setLogin(login); 
        this.setCpf(cpf);  
        this.setRoles(new ArrayList<Role>());
    }

    public ArrayList<Role> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<Role> roles) {
        this.roles = roles;
    }

    public Usuario() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Integer usuario_id) {
        this.usuario_id = usuario_id;
    }

    @Override
    public String toString() {
        return "Usuario [cpf=" + cpf + ", login=" + login + ", password=" + password
                + ", usuario_id=" + usuario_id + "]";
    }
 

    
    
}
