package com.processoseletivo.domain;


public class Colaborador {
    
    private Integer colaborador_id; 
    private String senha;
    private String nome;
    private String cpf;
    private Integer usuario_id;
    
    public Colaborador(Integer colaborador_id, String senha, String nome, String cpf, Integer usuario_id) {
        this.setColaborador_id(colaborador_id);
        this.setSenha(senha);
        this.setNome(nome);
        this.setCpf(cpf);
        this.setUsuario_id(usuario_id);
    }

    public Integer getColaborador_id() {
        return colaborador_id;
    }

    public void setColaborador_id(Integer colaborador_id) {
        this.colaborador_id = colaborador_id;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Integer getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Integer usuario_id) {
        this.usuario_id = usuario_id;
    }
    
}
